#############################################################################################
# Reference: https://docs.aws.amazon.com/eks/latest/userguide/cni-upgrades.html
#############################################################################################
locals {
  cni_plugin_namespace = "kube-system"
  cni_plugin_name      = "aws-node"
}

# Apply the manifest to the cluster
#    - does nothing on destroy
module "aws_k8s_cni_manifest_apply" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/kubernetes/kubectl-apply?ref=v1.0.11"

  enabled = var.enable_aws_k8s_cni_plugin

  manifest   = module.aws_k8s_cni_manifest_download.content
  kubeconfig = local.kubeconfig_file

  silence_apply_errors     = false # break terraform apply if apply fails
  enable_delete_on_destroy = false # no need to remove the SA annotation on terraform destroy
}

# When we upgrade de CNI plugin the SA is recreated and the IAM OIDC annotation is lost.
# This module will trigger when the upgrade happens and it will re-annotate the SA.
module "iam_role_for_aws_cni" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/aws/iam-role-for-k8s-service-account?ref=v1.0.17"

  project     = var.project
  environment = var.environment
  tags        = var.tags

  create            = var.enable_aws_k8s_cni_plugin
  create_iam_role   = true
  external_triggers = module.aws_k8s_cni_manifest_apply.apply_triggers

  aws_account_id = var.aws_account_id
  aws_region     = var.aws_region

  kubeconfig_file           = local.kubeconfig_file
  cluster_oidc_provider_url = var.oidc_provider_url

  k8s_service_account_namespace = local.cni_plugin_namespace
  k8s_service_account_name      = local.cni_plugin_name
  k8s_fully_defined_resources   = ["daemonset.apps/${local.cni_plugin_name}"]
  aws_iam_role_policy_arns      = ["arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"]

  depends_on = [module.aws_k8s_cni_manifest_apply]
}

# Download the manifest and save it to file
module "aws_k8s_cni_manifest_download" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/utils/download-content?ref=v1.0.0"

  enabled = var.enable_aws_k8s_cni_plugin

  source_url = var.aws_k8s_cni_manifest_url

  save_to_file     = true
  destination_file = "${var.config_output_path}/${var.aws_k8s_cni_manifest_file}"
}
