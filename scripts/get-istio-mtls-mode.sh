#!/usr/bin/env bash

set -e

usage() {
    echo "==> Need kubeconfig file as 1st arg"
    echo "==> Usage: files/scripts/get-istio-mtls-mode.sh <path to kubeconfig file>"
    exit 1
}

kubeconfig=${1}

[[ -z "${kubeconfig}" ]] && usage

mtls_mode=$( kubectl get meshpolicy default -o=jsonpath='{.spec.peers[0].mtls.mode}' --kubeconfig "${kubeconfig}" )

if [[ -z "${mtls_mode}" ]]; then
    # Strict mode is written "- mtls: {}" which means that empty string means strict mode
    echo '{"mode": "STRICT"}'
else
    echo ${mtls_mode} | sed -E "s/(.*)/{\"mode\": \"\1\"}/"
fi
