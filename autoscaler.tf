locals {
  autoscaler_namespace = "kube-system"
  autoscaler_name      = "cluster-autoscaler"

  autoscaler_iam_resource_name = "eks-${local.autoscaler_name}-${var.project}-${var.environment}-${var.aws_region}"

  autoscaler_helm_values = var.enable_autoscaler ? templatefile("${path.module}/files/templates/cluster-autoscaler-values.yaml.tpl", {
    name_override = local.autoscaler_name

    image_tag = var.autoscaler_container_image_tag

    region       = var.aws_region
    cluster_name = var.cluster_name

    service_account_iam_role_annotation = module.autoscaler_iam_role.this_iam_role_arn
  }) : ""
}

# Install autoscaler
resource "helm_release" "autoscaler" {
  count = var.enable_autoscaler ? 1 : 0

  name      = local.autoscaler_name
  namespace = local.autoscaler_namespace
  chart     = "stable/${local.autoscaler_name}"
  version   = var.autoscaler_helm_chart_version

  force_update = true

  values = concat([local.autoscaler_helm_values], var.autoscaler_helm_values)
}

# Create IAM role with required policies for autoscaler pod
module "autoscaler_iam_role" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version = "~> 3.0"

  create_role = var.enable_autoscaler

  role_name = local.autoscaler_iam_resource_name

  provider_url     = var.oidc_provider_url
  role_policy_arns = [try(aws_iam_policy.cluster_autoscaler[0].arn, "")]

  oidc_fully_qualified_subjects = ["system:serviceaccount:${local.autoscaler_namespace}:${local.autoscaler_name}"]

  tags = var.tags
}

# IAM Policy to allow autoscaler to scale ASGs
resource "aws_iam_policy" "cluster_autoscaler" {
  count = var.enable_autoscaler ? 1 : 0

  name_prefix = local.autoscaler_iam_resource_name
  description = "EKS cluster-autoscaler policy for cluster ${var.cluster_name}"
  policy      = try(data.aws_iam_policy_document.cluster_autoscaler[0].json, "")
}

data "aws_iam_policy_document" "cluster_autoscaler" {
  count = var.enable_autoscaler ? 1 : 0

  statement {
    sid    = "clusterAutoscalerAll"
    effect = "Allow"

    actions = [
      "autoscaling:DescribeAutoScalingGroups",
      "autoscaling:DescribeAutoScalingInstances",
      "autoscaling:DescribeLaunchConfigurations",
      "autoscaling:DescribeTags",
      "ec2:DescribeLaunchTemplateVersions",
    ]

    resources = ["*"]
  }

  statement {
    sid    = "clusterAutoscalerOwn"
    effect = "Allow"

    actions = [
      "autoscaling:SetDesiredCapacity",
      "autoscaling:TerminateInstanceInAutoScalingGroup",
      "autoscaling:UpdateAutoScalingGroup",
    ]

    resources = ["*"]

    condition {
      test     = "StringEquals"
      variable = "autoscaling:ResourceTag/kubernetes.io/cluster/${var.cluster_name}"
      values   = ["owned"]
    }

    condition {
      test     = "StringEquals"
      variable = "autoscaling:ResourceTag/k8s.io/cluster-autoscaler/enabled"
      values   = ["true"]
    }
  }
}
