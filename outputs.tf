output "calico_triggers" {
  value = sha1("${join(",", keys(local.calico_apply_triggers))}-${join(",", values(local.calico_apply_triggers))}")
}

output "ebs_csi_driver_helm_release" {
  value = helm_release.ebs_csi_driver
}

output "ebs_csi_driver_iam_role" {
  value = module.ebs_csi_driver_controller_iam_role.this_iam_role_arn
}
