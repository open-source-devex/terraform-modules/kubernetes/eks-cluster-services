apiVersion: crd.projectcalico.org/v1
kind: GlobalNetworkPolicy
metadata:
  name: ${policy_name}
spec:
  %{ if length(excluded_namespaces) > 0 }
  selector: "projectcalico.org/namespace not in { ${join(", ", excluded_namespaces)} }"
  %{ else }
  selector: all()
  %{ endif }
  types:
  - Egress
  egress:
  - action: Deny
    protocol: TCP
    source:
      selector: all()
    destination:
      nets:
      - 169.254.169.254/32
  - action: Allow
