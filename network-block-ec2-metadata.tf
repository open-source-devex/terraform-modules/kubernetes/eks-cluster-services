locals {
  # render policy template
  block_worker_instance_metadata_policy_name = "block-instance-metadata-${var.cluster_name}"
  block_worker_instance_metadata_manifest = local.network_block_ec2_metadata ? templatefile("${path.module}/files/templates/global-network-policy-block-instance-metadata.yaml.tpl", {
    policy_name         = local.block_worker_instance_metadata_policy_name
    excluded_namespaces = formatlist("'%s'", distinct(var.network_block_ec2_metadata_excluded_namespaces))
  }) : ""

  block_worker_instance_metadata_manifest_file_name = "${local.block_worker_instance_metadata_policy_name}.yaml"
  block_worker_instance_metadata_manifest_file      = join("", local_file.block_ec2_metadata.*.filename)

  network_block_ec2_metadata = var.network_block_ec2_metadata && var.enable_aws_k8s_cni_calico_plugin
}

# Apply the manifest to the cluster
#    - tries delete on destroy but does not produce any errors
module "block_ec2_metadata" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/kubernetes/kubectl-apply?ref=v1.0.11"

  enabled = local.network_block_ec2_metadata

  enable_delete_on_destroy = true # run kubectl delete on destroy (to not levae the iinstance of a calico CRD, which could break destroy of cni-calico)
  silence_delete_errors    = true # don't break terraform destroy if k8s in not responding well to delete

  silence_apply_errors = false # break terraform apply is apply fails

  # create a dependency between installation of calico cni and applying this policy
  apply_trigger = sha1(join(",", values(module.aws_k8s_cni_calico_manifest_apply.apply_triggers)))

  manifest   = local.block_worker_instance_metadata_manifest
  kubeconfig = local.kubeconfig_file

  detect_drift = true
}

# Save GlobalNetworkPolicy manifest to file
resource "local_file" "block_ec2_metadata" {
  count = local.network_block_ec2_metadata ? 1 : 0

  filename = "${var.config_output_path}/${local.block_worker_instance_metadata_manifest_file_name}"
  content  = local.block_worker_instance_metadata_manifest
}
