# Since we heavily depend on the content of this file
# it's better to make our own copy that only gets deleted when the module is destroyed
resource "local_file" "kubeconfig" {
  filename = "${var.config_output_path}/.module.open-source-devex.kubernetes.eks-cluster-services/kubeconfig-${var.project}-${var.environment}"
  content  = var.kubeconfig

  lifecycle {
    create_before_destroy = true # when re-creating, create before destroy
  }
}
