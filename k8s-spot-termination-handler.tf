locals {
  k8s_spot_termination_handler_helm_values = var.enable_spot_instance_handler ? templatefile("${path.module}/files/templates/k8s-spot-termination-handler-values.yaml.tpl", {
    image_tag    = var.k8s_spot_termination_handler_container_image_tag
    cluster_name = var.cluster_name
  }) : ""
}

resource "helm_release" "k8s_spot_termination_handler" {
  count = var.enable_spot_instance_handler ? 1 : 0

  name      = "k8s-spot-termination-handler"
  namespace = "kube-system"
  chart     = "stable/k8s-spot-termination-handler"
  version   = var.k8s_spot_termination_handler_helm_chart_version

  force_update = true

  values = concat([local.k8s_spot_termination_handler_helm_values], var.k8s_spot_termination_handler_helm_values)
}
